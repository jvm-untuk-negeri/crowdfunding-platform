<?php

return array(

    /*
	|--------------------------------------------------------------------------
	| Errors Language Lines
	|--------------------------------------------------------------------------
	|
	*/

    "error_404"         => "404 Halaman tidak ditemukan",
    "error_403"         => "Kesalahan 403",
    "error_406"         => "Kesalahan 406",

    "error_404_description"         => "Ups! Halaman tidak ditemukan.",
    "error_404_subdescription"      => "Maaf, halaman itu tidak ditemukan!",
    "error_403_description"         => "Maaf, akses dilarang!",
    "error_406_description"         => "Maaf, tidak dapat diterima!",


    "error_user_banned"             => "Maaf, pengguna ini telah dilarang!",
    "error_user_delete"             => "Maaf, pengguna ini telah dihapus!",
    "go_home"                       => "Pergi ke halaman utama",

    "error_required_mail"           => "Kesalahan! Email Anda diperlukan, Buka pengaturan aplikasi dan hapus aplikasi kami dan coba lagi",
    "mail_exists"                   => 'Kesalahan! email tersebut telah digunakan oleh pengguna lain. ',
);
