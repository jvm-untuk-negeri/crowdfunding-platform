<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Slider Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'slider_1_title' => 'Sedikit bantuan, untuk tujuan besar!',
    'slider_1_subtitle' => '',

    'slider_2_title' => 'Bantu orang di dekat Anda',
    'slider_2_subtitle' => '',

    'slider_3_title' => 'Mendidik masa depan',
    'slider_3_subtitle' => '',

];
