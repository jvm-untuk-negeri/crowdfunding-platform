<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Baris-baris bahasa untuk autentifikasi
    |--------------------------------------------------------------------------
    |
    | Baris bahasa berikut digunakan selama proses autentifikasi untuk beberapa
    | pesan yang perlu kita tampilkan ke pengguna. Anda bebas untuk memodifikasi
    | baris bahasa sesuai dengan keperluan aplikasi anda.
    |
    */

    'email_sent'                => 'Email untuk mengatur ulang kata sandi telah dikirim.',
    'reset_password'            => 'Setel Ulang Kata Sandi',
    'enter_email_pass'          => 'Masukkan email Anda dan kata sandi baru',
    'email_has_been_set'        => 'Berhasil dikirim, periksa email Anda',
    'or_sign_in_with'           => 'Atau Masuk dengan ...',
    'or_sign_up_with'           => 'Atau Daftar dengan ...',
    'username_or_email'         => 'Username atau Email',
    'username'                  => 'Username',
    'full_name'                 => 'Nama lengkap',
    'team_name'                 => 'Nama tim',
    'status'                    => 'Status',
    'login'                     => 'Masuk',
    'lupa_password'             => 'Lupa Kata Sandi?',
    'confirm_password'          => 'Konfirmasi Kata Sandi',
    'sign_up'                   => 'Daftar',
    'sign_in'                   => 'Masuk',
    'remember_me'               => 'Ingat Saya',
    'logout'                    => 'Keluar',
    'back'                      => 'Kembali',
    'password_reset'            => 'Kata sandi Anda telah berhasil diatur ulang.',
    'password_recover'          => 'Pemulihan Kata Sandi',
    'password_reset_2'          => 'Reset Kata Sandi',
    'email'                     => 'Email',
    'password'                  => 'Password',
    'password_confirmation'     => 'Konfirmasi kata sandi',
    'send'                      => 'Kirim',
    'password_reset_mail'       => 'Untuk mengatur ulang kata sandi Anda, lengkapi formulir ini:',
    'login_required'            => 'Anda harus login untuk melanjutkan',
    'check_account'             => 'Selamat! periksa email Anda untuk mengaktifkan akun Anda, jika Anda tidak melihat email tersebut periksa folder SPAM Anda ',
    'success_update'            => 'Akun Anda telah diperbarui dengan sukses',
    'success_update_password'   => 'Kata Sandi Anda telah berhasil diperbarui',
    'login'                     => 'Masuk',
    'not_have_account'          => "Belum punya akun?",
    'ready_have_an_account '    => 'Sudah punya akun? ',
    'terms'                     => 'Setelah Anda mengklik untuk Mendaftar, Anda setuju dengan Syarat dan Ketentuan kami',
    'logs_in_comments'          => 'Anda harus login untuk mengomentari bidikan ini',
    'error_captcha'             => 'Captcha Error',
    'pay_register'              => 'Bayar dan daftar',
    'success_account'           => 'Akun Anda telah berhasil dibuat, Silakan masuk',
    'error_desc'                => 'Ups! Ada beberapa masalah dengan masukan Anda. ',
    'error_logging'             => 'Kredensial ini tidak cocok dengan catatan kami.',
];
