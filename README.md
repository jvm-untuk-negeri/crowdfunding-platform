# Tentang Crowdfunding Platform

Crowdfunding Platform merupakan aplikasi untuk membuat platform penggalangan dana. Proyek ini muncul karena adanya kebutuhan dari Pemerintah Daerah Kabupaten Banyumas akan platform penggalangan dana seperti kitabisa.com khusus bagi warga Banyumas.

# Ikut Berkontribusi Dan Kembangkan Bersama

Proyek ini diprakarsai oleh JVM dalam program CSR perusahaan. Walaupun demikian, proyek ini terbuka bagi siapa saja yang ingin ikut mengembangkan platform tersebut. Kamu dapat ikut mengembangkan melalui berbagai macam cara.

## Melaporkan masalah ataupun usulan pengembangan

Kamu dapat ikut melaporkan masalah yang ada pada aplikasi. Selain itu, kamu juga bisa mengusulkan fitur-fitur yang masih bisa dikembangkan pada aplikasi.

## Urun kode

Bagi kamu yang bisa Laravel Framework, kamu bisa ikut berkontribusi dengan kode yang kamu buat.

## Dokumentasi

Hal yang sering terlewatkan dari proyek-proyek pengembangan software adalah dokumentasi. Kami membuka pintu sebesar-besarnya bagi kamu yang ingin berkontribusi dalam dokumentasi aplikasi.

# Lisensi

Crowdfunding Platform dikembangkan menggunakan Laravel Framework yang merupakan software open-source di bawah [Lisensi MIT](https://opensource.org/licenses/MIT).

